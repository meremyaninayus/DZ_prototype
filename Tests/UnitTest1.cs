using DZ_Prototype;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        User _user;
        [SetUp]
        public void Setup()
        {
            _user = new User
            {
                FirstName = "����",
                LastName = "������"
            };
        }

        [Test]
        public void TestDocumentCopy()
        {
            var doc = new Document(theme: "������", author: _user);
            var copyDoc = doc.Copy();
            Assert.IsTrue(CheckDocumentCopy(doc, copyDoc));
        }

        [Test]
        public void TestMemoDocumentCopy()
        {
            var doc = new MemoDocument(theme: "��", author: _user, signer: _user);
            var copyDoc = doc.Copy();
            Assert.IsTrue(CheckDocumentCopy(doc, copyDoc)
                && CheckMemoDocumentCopy(doc, copyDoc));
        }

        [Test]
        public void TestHRMemoDocumentCopy()
        {
            var doc = new HRMemoDocument(theme: "��", author: _user, signer: _user, additionalPayment: true);
            var copyDoc = doc.Copy();
            Assert.IsTrue(CheckDocumentCopy(doc, copyDoc)
                && CheckMemoDocumentCopy(doc, copyDoc)
                && CheckHRMemoDocumentCopy(doc, copyDoc));
        }


        [Test]
        public void TestDocumentClone()
        {
            var doc = new Document(theme:"������", author: _user);
            var copyDoc = (Document)doc.Clone();
            Assert.IsTrue(CheckDocumentCopy(doc, copyDoc));
        }

        [Test]
        public void TestMemoDocumentClone()
        {
            var doc = new MemoDocument(theme:"��", author:_user, signer:_user);
            var copyDoc = (MemoDocument)doc.Clone();
            Assert.IsTrue(CheckDocumentCopy(doc, copyDoc)
                && CheckMemoDocumentCopy(doc, copyDoc));
        }

        [Test]
        public void TestHRMemoDocumentClone()
        {
            var doc = new HRMemoDocument(theme:"��", author:_user, signer:_user, additionalPayment:true);
            var copyDoc = (HRMemoDocument)doc.Clone();
            Assert.IsTrue(CheckDocumentCopy(doc, copyDoc)
                && CheckMemoDocumentCopy(doc, copyDoc)
                && CheckHRMemoDocumentCopy(doc, copyDoc));
        }

        private bool CheckDocumentCopy(Document orig, Document copy)
        {
            return (copy != orig &&
                copy.Author == orig.Author &&
                copy.Theme == orig.Theme &&
                copy.CreatedAt != orig.CreatedAt
                );        
        }
        private bool CheckMemoDocumentCopy(MemoDocument orig, MemoDocument copy)
        {
            return (orig.Signer == copy.Signer);
        }

        private bool CheckHRMemoDocumentCopy(HRMemoDocument orig, HRMemoDocument copy)
        {
            return (orig.AdditionalPayment == copy.AdditionalPayment);
        }
    }
}