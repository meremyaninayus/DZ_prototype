﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_Prototype
{
    interface IMyCloneable<T>
    {
        T Copy();
    }
}
