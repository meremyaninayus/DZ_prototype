﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_Prototype
{
    public class HRMemoDocument: MemoDocument, IMyCloneable<Document>, ICloneable
    {
        public string HRComment {get; set;}
        public bool AdditionalPayment { get; set; }

        public HRMemoDocument(string theme, User author, User signer, bool additionalPayment):base(theme, author, signer)
        {
            AdditionalPayment = additionalPayment;
        }

        public override HRMemoDocument Copy()
        {
            var newDoc = new HRMemoDocument(Theme, Author, Signer, AdditionalPayment);
            return newDoc;
        }

        public override object Clone()
        {
            return this.Copy();
        }

    }
}
