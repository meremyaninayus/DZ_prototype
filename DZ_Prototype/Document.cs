﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;

namespace DZ_Prototype
{
    public class Document : IMyCloneable<Document>,ICloneable
    {
        public string Number { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Theme { get; set; }
        public User Author { get; set; }

        public Document()
        {
            CreatedAt = DateTime.Now;
        }

        public Document(string theme, User author):this()
        {
            Theme = theme;
            Author = author;
        }


        public virtual Document Copy()
        {
            var newDoc = new Document(Theme, Author);
            return newDoc;
        }


        public virtual object Clone()
        {
            return this.Copy();
        }
    }
}
