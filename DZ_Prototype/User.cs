﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_Prototype
{
    public class User
    {
        private readonly int _id;
        public int Id { get { return _id; } }
        public string FirstName { get; set;}
        public string LastName { get; set; }

        public User()
        {
            _id = 1;
        }
    }
}
