﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_Prototype
{
    public class MemoDocument : Document, IMyCloneable<Document>, ICloneable
    {
        public User Signer { get; set; }
        public IEnumerable<User> Receivers { get; set; }

        public MemoDocument(string theme, User author, User signer):base(theme, author)
        {
            Signer = signer;
        }

        public override MemoDocument Copy()
        {
            var newDoc =  new MemoDocument(Theme, Author, Signer);
            return newDoc;
        }

        public override object Clone()
        {
            return this.Copy();
        }

    }
}
